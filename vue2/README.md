<!--
 * @FilePath: README.md
 * @Author: ljy
 * @Date: 2022-12-12 14:58:33
 * @LastEditors: 
 * @LastEditTime: 2022-12-12 23:46:47
 * Copyright: 2022 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 
-->
# vue2

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn start
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
