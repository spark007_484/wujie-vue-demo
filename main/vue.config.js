/*
 * @FilePath: vue.config.js
 * @Author: ljy
 * @Date: 2022-12-12 14:58:33
 * @LastEditors: 
 * @LastEditTime: 2022-12-13 00:09:47
 * Copyright: 2022 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 
 */
// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/demo-main/" : "/",
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
    open: process.env.NODE_ENV === "development",
    port: "8000",
  },
  transpileDependencies: [
    "sockjs-client",
  ],
};
