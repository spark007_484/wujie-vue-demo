/*
 * @FilePath: hostMap.js
 * @Author: ljy
 * @Date: 2022-12-12 14:58:33
 * @LastEditors: 
 * @LastEditTime: 2022-12-13 00:09:10
 * Copyright: 2022 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 
 */
const map = {
  "//localhost:7200/": "//wujie-micro.github.io/demo-vue2/",
  "//localhost:7300/": "//wujie-micro.github.io/demo-vue3/",
  "//localhost:7500/": "//wujie-micro.github.io/demo-vite/",
  "//localhost:8000/": "//wujie-micro.github.io/demo-main/",
};

export default function hostMap(host) {
  if (process.env.NODE_ENV === "production") return map[host];
  return host;
}
